package ru.t1.panasyuk.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskStartByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskStartByIndexResponse;
import ru.t1.panasyuk.tm.event.ConsoleEvent;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class TaskStartByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Start task by index.";

    @NotNull
    private static final String NAME = "task-start-by-index";

    @Override
    @EventListener(condition = "@taskStartByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken(), index);
        @NotNull final TaskStartByIndexResponse response = taskEndpoint.startTaskByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}