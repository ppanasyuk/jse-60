package ru.t1.panasyuk.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @NotNull
    private static final String DESCRIPTION = "Close application.";

    @NotNull
    private static final String NAME = "exit";

    @Override
    @EventListener(condition = "@applicationExitListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.exit(0);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}