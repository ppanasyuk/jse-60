package ru.t1.panasyuk.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResultResponse {

    public AbstractTaskResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}