package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = String.format(
                "DELETE FROM %s m WHERE m.%s.%s = :%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_USER_ID
        );
        entityManager
                .createQuery(jpql)
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s.%s = :%s ORDER BY m.%s DESC",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_CREATED
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll(userId);
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s.%s = :%s ORDER BY m.%s DESC",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_USER_ID,
                getSortType(comparator)
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s.%s = :%s AND m.%s = :%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_ID
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .setParameter(FieldConst.FIELD_ID, id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null) return null;
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s.%s = :%s ORDER BY m.%s DESC",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_CREATED
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .setFirstResult(index - 1)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = String.format(
                "SELECT COUNT(m) FROM %s m WHERE m.%s.%s = :%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER,
                FieldConst.FIELD_ID,
                FieldConst.FIELD_USER_ID
        );
        return entityManager
                .createQuery(jpql, Long.class)
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .getSingleResult()
                .intValue();
    }

}