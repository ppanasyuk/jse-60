package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.api.service.dto.ITaskDtoService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, ITaskDtoRepository>
        implements ITaskDtoService {

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(userId, task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<TaskDTO> models;
        @NotNull final ITaskDtoRepository repository = getRepository();
        models = repository.findAll(userId, comparator);
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<TaskDTO> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<TaskDTO> tasks;
        @NotNull final ITaskDtoRepository repository = getRepository();
        tasks = repository.findAllByProjectId(userId, projectId);
        return tasks;
    }

    @Override
    @Transactional
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final ITaskDtoRepository repository = getRepository();
        repository.removeAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

}