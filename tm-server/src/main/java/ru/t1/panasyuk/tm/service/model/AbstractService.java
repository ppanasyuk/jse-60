package ru.t1.panasyuk.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.model.IRepository;
import ru.t1.panasyuk.tm.api.service.model.IService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>>
        implements IService<M> {

    @Getter
    @NotNull
    @Autowired
    private R repository;

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        @NotNull final R repository = getRepository();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final R repository = getRepository();
        repository.add(models);
        return models;
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final R repository = getRepository();
        repository.clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        boolean result;
        @NotNull final R repository = getRepository();
        result = repository.findOneById(id) != null;
        return result;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @Nullable final List<M> models;
        @NotNull final R repository = getRepository();
        models = repository.findAll();
        return models;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null) return null;
        @Nullable final M model;
        @NotNull final R repository = getRepository();
        model = repository.findOneById(id);
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize()) throw new IndexIncorrectException();
        @Nullable final M model;
        @NotNull final R repository = getRepository();
        model = repository.findOneByIndex(index);
        return model;
    }

    @Override
    public int getSize() {
        int result;
        @NotNull final R repository = getRepository();
        result = repository.getSize();
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public M remove(@NotNull final M model) {
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        removedModel = repository.findOneById(model.getId());
        if (removedModel == null) throw new EntityNotFoundException();
        repository.remove(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        removedModel = repository.findOneById(id);
        repository.remove(removedModel);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.remove(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize()) throw new IndexIncorrectException();
        @Nullable final M removedModel;
        @NotNull final R repository = getRepository();
        removedModel = repository.findOneByIndex(index);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.remove(removedModel);
        return removedModel;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final R repository = getRepository();
        repository.update(model);
    }

}