package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.panasyuk.tm.api.service.dto.ISessionDtoService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository>
        implements ISessionDtoService {

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @Nullable final List<SessionDTO> sessions;
        @NotNull final ISessionDtoRepository repository = getRepository();
        sessions = repository.findAll();
        return sessions;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO remove(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final ISessionDtoRepository repository = getRepository();
        repository.remove(session);
        return session;
    }

}