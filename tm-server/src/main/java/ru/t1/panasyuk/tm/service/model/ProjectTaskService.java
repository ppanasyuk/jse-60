package ru.t1.panasyuk.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.model.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.model.ITaskRepository;
import ru.t1.panasyuk.tm.api.service.model.IProjectTaskService;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @Getter
    @NotNull
    @Autowired
    protected IProjectRepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    protected ITaskRepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public Task bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        @Nullable final Project project;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        project = projectRepository.findOneById(userId, projectId);
        boolean isExist = project != null;
        if (!isExist) throw new ProjectNotFoundException();
        task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(project);
        taskRepository.update(task);
        return task;
    }

    @Override
    @Transactional
    public Project removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable Project project;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
        return project;
    }

    @Override
    @Transactional
    public Project removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final Project project;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
        project = projectRepository.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        removeProjectById(userId, project.getId());
        return project;
    }

    @Override
    @Transactional
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<Project> projects;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        projects = projectRepository.findAll(userId);
        if (projects == null) return;
        for (@NotNull final Project project : projects) removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    @Transactional
    public Task unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        boolean isExist = projectRepository.findOneById(userId, projectId) != null;
        if (!isExist) throw new ProjectNotFoundException();
        task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskRepository.update(task);
        return task;
    }

}